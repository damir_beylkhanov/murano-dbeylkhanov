/**
 * Module dependencies.
 * @private
 */
var path = require('path');
var webpack = require('webpack');
var cssnano = require('cssnano');
var autoprefixer = require('autoprefixer');
var ExtractText = require('extract-text-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

/**
 * Environment constants.
 * @private
 */
var ENV_DEV = 'dev';
var ENV_PROD = 'prod';

/**
 * Module constants.
 * @private
 */
var env = process.argv.indexOf('-p') === -1 ? ENV_DEV : ENV_PROD;
var srcDir = path.resolve(__dirname, 'src');
var distDir = path.resolve(__dirname, '../wwwroot');
var extractSass = new ExtractText({
    filename: "assets/[name].css",
    disable: env === ENV_DEV
});

/**
 * Production plugins
 * @type {Array}
 * @constant
 * @private
 */
var prodPlugins = [
    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
            drop_console: true,
            unsafe: true
        }
    }),
    new OptimizeCssAssetsPlugin({
        assetNameRegExp: /.*\.css$/g,
        cssProcessor: cssnano,
        cssProcessorOptions: {
            discardComments: {
                removeAll: true
            }
        },
        canPrint: true
    })
];

/**
 * Module configuration
 * @type {Object}
 * @constant
 * @private
 */
var moduleConfig = {
    rules: [
        {
            test: /\.scss$/,
            exclude: /node_modules/,
            use: extractSass.extract({
                use: [
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ],
                fallback: 'style-loader'
            })
        },
        {
            test: /\.css$/,
            exclude: /node_modules/,
            use: [
                'style-loader',
                'css-loader'
            ]
        },
        {
            test: /\.ts$/,
            exclude: /node_modules/,
            use: [
                'ts-loader'
            ]
        },
        {
            test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
            exclude: /node_modules/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'assets/[hash:8].[ext]'
                }
            }]
        }
    ]
};

/**
 * Plugins configuration
 * @type {[*]}
 * @constant
 * @private
 */
var plugins = [
    extractSass,
    autoprefixer
];

if (env === ENV_PROD) {
    prodPlugins.forEach(function (plugin) {
        plugins.push(plugin);
    });
}

/**
 * Webpack configuration
 * @module WebpackConfig
 */
module.exports = {
    devtool: env === ENV_DEV ? 'source-map' : false,
    context: srcDir,
    entry: {
        app: './index.ts'
    },
    output: {
        filename: 'assets/[name].js',
        path: distDir,
        publicPath: '/'
    },
    module: moduleConfig,
    plugins: plugins,
    resolve: {
        extensions: ['.js', '.ts', '.json']
    }
};
