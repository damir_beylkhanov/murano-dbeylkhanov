import {
    getElement,
    eventOn
} from './DomUtils';

/**
 * @class ScreenComponent
 * @public
 */
export default class ScreenComponent {

    private screen: Element;
    private video: HTMLVideoElement;
    private offset: ClientRect;
    private process: boolean;
    private currentScrollValue: number;
    private scrollHandler: Function;

    /**
     * @constructor
     * @param {Element} screenElement
     * @public
     */
    public constructor(screenElement: Element) {
        this.screen = screenElement;
        this.video = <HTMLVideoElement>getElement('.video', this.screen);
        this.offset = screenElement.getBoundingClientRect();
        this.process = false;
        this.currentScrollValue = 0;
        this.bindEventHandlers();
    }

    /**
     * @method setScrollHandler
     * @param {Function} scrollHandler
     * @public
     */
    public setScrollHandler(scrollHandler: Function): void {
        this.scrollHandler = scrollHandler;
    }

    /**
     * @function autoScroll
     * @private
     */
    private autoScroll() {
        if (this.process) {
            return;
        }
        this.process = true;
        this.video.pause();
        this.scrollHandler && this.scrollHandler(this.offset.height - 64, 600, () => {
            this.video.play();
            this.process = false;
        });
    }

    /**
     * Bind all event handlers
     * @method bindEventHandlers
     * @private
     */
    private bindEventHandlers(): void {

        let buttonScrollDown: Element = getElement('.helper--scroll-down', this.screen);
        buttonScrollDown && eventOn(buttonScrollDown, 'click', (e: Event) => {
            e.preventDefault();
            this.autoScroll();
        });

        let scrollHandler: EventListener = (e: Event) => {
            if (this.process) {
                e.preventDefault();
            }
        };

        eventOn(window, 'touchstart', scrollHandler);
        eventOn(window, 'mousewheel', scrollHandler);
        eventOn(window, 'scroll', scrollHandler);

    }

}
