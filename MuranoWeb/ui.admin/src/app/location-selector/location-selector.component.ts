import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {OfficeLocationsService} from '../services/office.locations.service';
import {OfficeLocation} from '../services/office.location';

@Component({
  selector: 'app-location-selector',
  templateUrl: './location-selector.component.html',
  styleUrls: ['./location-selector.component.css']
})
export class LocationSelectorComponent implements OnInit {

  @Input()
  currentLocation: OfficeLocation;

  @Input()
  defaultLabel: string;

  @Input()
  firstAsDefault: boolean;

  @Output()
  locationSelect: EventEmitter<OfficeLocation> = new EventEmitter<OfficeLocation>();

  locations: OfficeLocation[];
  constructor(private service: OfficeLocationsService) { }

  ngOnInit() {
    this.service.getLocations().then(v => {
      this.locations = v.slice();
      if (this.defaultLabel) {
        this.locations.unshift({
          code: null,
          name: this.defaultLabel
        } as OfficeLocation);
      }
      if (this.firstAsDefault) {
        this.locationChange(this.locations[0]);
      }
    });
  }

  locationChange(location: OfficeLocation) {
    this.currentLocation = location;
    this.locationSelect.emit(location);
  }

}
