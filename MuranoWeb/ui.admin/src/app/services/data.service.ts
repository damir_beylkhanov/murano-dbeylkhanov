import { Headers, Http } from '@angular/http';

export class DataService<T> {
    private headers: Headers = new Headers({'Content-Type': 'application/json'});

    constructor(protected http: Http, protected url: string) { }

    public getAll(conditions?: { [s: string]: string; }): Promise<T[]> {
        return this.http.get(this.url, {search: conditions})
                        .toPromise()
                        .then(response => response.json() as T[])
                        .catch(this.handleError);
    }

    public getById(id: number): Promise<T> {
        return this.http.get(`${this.url}/${id}`)
                        .toPromise()
                        .then(response => response.json() as T)
                        .catch(this.handleError);
    }

    public add(item: T): Promise<T> {
        return this.http.post(this.url, JSON.stringify(item), {headers: this.headers})
                        .toPromise()
                        .then(response => response.json() as T)
                        .catch(this.handleError);
    }

    public update(id: number, vacancy: T): Promise<T> {
        return this.http.put(`${this.url}/${id}`, JSON.stringify(vacancy), {headers: this.headers})
                        .toPromise()
                        .then(response => response.json() as T)
                        .catch(this.handleError);
    }

    protected handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
