﻿export class Icon {
  id: number;
  url: string;
  title: string;
}
