﻿import { DataService } from './data.service';
import { Icon } from './icon';
import { Http, RequestOptionsArgs } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class IconService extends DataService<Icon> {
    constructor(http: Http) {
        super(http, 'admin/api/icons');
    }

    public delete(id: number): Promise<any> {
        return this.http.delete(`${this.url}/${id}`).toPromise().catch(super.handleError);
    }

    public uploadIcons(files: FileList): Promise<any> {
        let args: RequestOptionsArgs = <RequestOptionsArgs>{
            headers: new Headers({
                'Content-Type': 'multipart/form-data; charset=UTF-8'
            })
        };
        return this.http.post(`${this.url}`, this.makeFormData(files), args)
            .toPromise()
            .catch(this.handleError);
    }

    public getIcons(): Promise<Icon[]> {
        return this.http.get(`${this.url}`)
            .toPromise()
            .then(response => response.json() as Icon[])
            .catch(this.handleError);
    }

    public deleteIcon(icon: Icon) {
        return this.http.delete(`${this.url}/${icon.id}`)
            .toPromise()
            .catch(this.handleError);
    }

    public updateIcon(icon: Icon) {
        return this.update(icon.id, icon)
            .catch(this.handleError);
    }

    public replaceIcon(icon: Icon, files: FileList) {
        let args: RequestOptionsArgs = <RequestOptionsArgs>{
            headers: new Headers({
                'Content-Type': 'multipart/form-data; charset=UTF-8'
            })
        };
        return this.http.post(`${this.url}/${icon.id}`, this.makeFormData(files), args)
            .toPromise()
            .catch(this.handleError);
    }

    private makeFormData(files: FileList): FormData {
        let formData: FormData = new FormData();
        for (let i: number = 0; i < files.length; i++) {
            formData.append('files', files[i], files[i].name);
        }
        return formData;
    }
}
