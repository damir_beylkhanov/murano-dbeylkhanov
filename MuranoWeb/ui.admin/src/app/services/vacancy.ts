﻿import { OfficeLocation } from './office.location';
import { Icon } from './icon';
export class Vacancy {
    id: number;
    title: string;
    location: string;
    summary: string;
    description: string;
    requirements: string;
    inArchive: boolean;
    iconId: number;
    availableLocations: OfficeLocation[];
    availableIcons: Icon[];
}
