﻿import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { Vacancy } from '../services/vacancy';
import { VacanciesService } from '../services/vacancies.service';
import { OfficeLocationsService } from '../services/office.locations.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/forms';
import { Location } from '@angular/common';
import { OfficeLocation } from '../services/office.location';
import { Icon } from '../services/icon';
import { IconService } from '../services/icons.service';
import { TinymceModule } from 'angular2-tinymce';

@Component({
  selector: 'app-vacancy-edit',
  templateUrl: './vacancy-edit.component.html',
  styleUrls: ['./vacancy-edit.component.css']
})
export class VacancyEditComponent implements OnInit {
  vacancy: Vacancy;
  locations: OfficeLocation[];
  icons: Icon[];

  constructor(private service: VacanciesService, private locationsService: OfficeLocationsService, private iconService: IconService, private route: ActivatedRoute, private location: Location) { }

  save(vacancyForm: NgForm): void {
    if (vacancyForm.valid) {
      this.service.save(this.vacancy).then(() => this.location.back());
    }
  }

  saveAsNew(vacancyForm: NgForm): void {
    if (vacancyForm.valid) {
      delete this.vacancy.id;
      this.service.save(this.vacancy).then(() => this.location.back());
    }
  }

  toArchive(): void {
    this.service.toArchive(this.vacancy.id).then(() => this.location.back());
  }

  openVacancy(): void {
    this.service.fromArchive(this.vacancy.id).then(() => this.location.back());
  }

  back() {
    this.location.back();
  }

  ngOnInit() {
    this.locationsService.getLocations().then(v => {
      this.locations = v;
      if (this.vacancy) {
        this.vacancy.availableLocations = this.locations;
      }
      });

    this.iconService.getIcons().then(v => {
        this.icons = v;
        if (this.vacancy) {
            this.vacancy.availableIcons = this.icons;
        }
    });

    this.route.params.subscribe(params => {
       const id = +params['id'];
       if (id) {
           this.service.getById(id).then(v => { this.vacancy = v; this.vacancy.availableIcons = this.icons; this.vacancy.availableLocations = this.locations; });
       } else {
        this.vacancy = new Vacancy();
        this.vacancy.availableLocations = this.locations;
        this.vacancy.availableIcons = this.icons;
       }
    });
  }
}
