﻿import { Routes } from '@angular/router';
import { VacanciesListComponent } from './vacancies-list/vacancies-list.component';
import { JobApplicationsListComponent } from './job-applications-list/job-applications-list.component';
import { VacancyEditComponent } from './vacancy-edit/vacancy-edit.component';
import { PhotosListComponent } from './photos-list/photos-list.component';
import { QuotesListComponent } from './quotes-list/quotes-list.component';
import { IconsListComponent } from './icons-list/icons-list.component';

export const AppRoutes: Routes = [
    { path: 'vacancies/opened',      component: VacanciesListComponent, data: { inArchive: false, title: 'Opened Vacancies' } },
    { path: 'vacancies/archive',     component: VacanciesListComponent, data: { inArchive: true, title: 'Archived Vacancies' } },
    { path: 'vacancies/opened/:id',  component: VacancyEditComponent, data: { title: 'Edit Vacancy' } },
    { path: 'vacancies/:id',         component: VacancyEditComponent, data: { title: 'Edit Vacancy' } },
    { path: 'vacancies/archive/:id', component: VacancyEditComponent, data: { title: 'Edit Vacancy' } },
    { path: 'vacancies/new',         component: VacancyEditComponent, data: { title: 'Add Vacancy' } },
    { path: 'job-applications',      component: JobApplicationsListComponent, data: { title: 'Job Applications' } },
    { path: 'photos',                component: PhotosListComponent, data: { title: 'Photos' } },
    { path: 'quotes',                component: QuotesListComponent, data: { title: 'Quotes' } },
    { path: 'icons',                 component: IconsListComponent, data: { title: 'Icons' } },

    { path: '', redirectTo: 'vacancies/opened', pathMatch: 'full' },
    { path: 'vacancies', redirectTo: 'vacancies/opened', pathMatch: 'full' }
];
