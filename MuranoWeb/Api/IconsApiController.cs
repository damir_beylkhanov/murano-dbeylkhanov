﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MuranoWeb.Data.Entity;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;

namespace MuranoWeb.Api
{
    [Route("admin/api/icons")]
    public class IconsApiController : Controller
    {
        private readonly IHostingEnvironment _appEnvironment;
        private readonly IRepository<VacancyIcon> _imagesRepository;

        public IconsApiController(IHostingEnvironment appEnvironment, IRepository<VacancyIcon> imagesRepository)
        {
            _appEnvironment = appEnvironment;
            _imagesRepository = imagesRepository;
        }

        [HttpGet]
        public Task<IconModel[]> GetImages()
        {
            return _imagesRepository.GetAll().Select(x => new IconModel
            {
                Id = x.Id,
                Url = ImagesHelper.GetIconUrl(x.Name),
                Title = x.Title,
            }).ToArrayAsync();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] IconModel model)
        {
            if (model == null || model.Id != id || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var icon = await _imagesRepository.GetById(id);
            icon.Title = model.Title;

            await _imagesRepository.Update(icon);

            return new NoContentResult();
        }


        [HttpPost]
        public async Task<IActionResult> Upload(List<IFormFile> files)
        {
            string directory = getDirectory();
            var images = new List<VacancyIcon>();
            var count = _imagesRepository.GetAll().Count(x => true);
            if (files.Count == 1)
            {
                var file = files[0];
                if (file.Length > 0)
                {
                    var fileName = ImagesHelper.RenameIfExist(directory, file.FileName);
                    using (var stream = new FileStream(Path.Combine(directory, fileName), FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }

                    images.Add(new VacancyIcon { Name = fileName, Title = @"Icon" });
                }
            }
            if (images.Any())
            {
                await _imagesRepository.AddRange(images);
            }

            return Ok();
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> Replace(int id, List<IFormFile> files)
        {
            string directory = getDirectory();
            var images = new List<VacancyIcon>();
            var count = _imagesRepository.GetAll().Count(x => true);
            if (files.Count == 1)
            {
                var file = files[0];
                if (file.Length > 0)
                {
                    var fileName = ImagesHelper.RenameIfExist(directory, file.FileName);
                    using (var stream = new FileStream(Path.Combine(directory, fileName), FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }

                    var icon = await _imagesRepository.GetById(id);
                    icon.Name = fileName;
                    await _imagesRepository.Update(icon);
                }
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var image = await _imagesRepository.GetById(id);
            if (image == null)
            {
                return NotFound();
            }

            FileHelper.DeleteFile(getDirectory(), image.Name);

            await _imagesRepository.Remove(image);
            return Ok();
        }

        private string getDirectory()
        {
            return ImagesHelper.GetIconsDirectory(_appEnvironment.WebRootPath);
        }


    }
}
