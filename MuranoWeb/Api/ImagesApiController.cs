﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MuranoWeb.Data.Entity;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/images")]
    public class ImagesApiController : Controller
    {
        private readonly IHostingEnvironment _appEnvironment;
        private readonly IRepository<Image> _imagesRepository;

        public ImagesApiController(IHostingEnvironment appEnvironment, IRepository<Image> imagesRepository)
        {
            _appEnvironment = appEnvironment;
            _imagesRepository = imagesRepository;
        }

        [HttpGet("{location}")]
        public Task<ImageModel[]> GetImages(Location location)
        {
            return _imagesRepository.GetAll().Where(x => x.Location == location).OrderBy(x => x.SortOrder).Select(x => new ImageModel
            {
                Id = x.Id,
                Show = x.Show,
                Url = ImagesHelper.GetImageUrl(x.Name, location)
            }).ToArrayAsync();
        }

        [HttpPost("{location}")]
        public async Task<IActionResult> Upload(Location location, List<IFormFile> files)
        {
            string directory = getDirectory(location);
            var images = new List<Image>();
            var count = _imagesRepository.GetAll().Count(x => x.Location == location);
            foreach (IFormFile file in files)
            {
                if (file.Length > 0)
                {
                    var fileName = ImagesHelper.RenameIfExist(directory, file.FileName);
                    using (var stream = new FileStream(Path.Combine(directory, fileName), FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }

                    var previewFileName = ImagesHelper.GeneratePreview(_appEnvironment.WebRootPath, fileName, location);

                    images.Add(new Image{Location = location, Name = fileName, PreviewName = previewFileName, SortOrder = count++});
                }
            }
            if (images.Any())
            {
                await _imagesRepository.AddRange(images);
            }

            return Ok();
        }
        /*
        [HttpDelete("{location}/{id}")]
        public async Task<IActionResult> Delete(Location location, int id)
        {
            var image = await _imagesRepository.GetById(id);
            if (image == null)
            {
                //return NotFound();
                return new JsonResult("Not found. Location: "+location.ToString()+" id="+id);
            }
            FileHelper.DeleteFile(getDirectory(location), image.Name);
            FileHelper.DeleteFile(getPreviewDirectory(location), image.PreviewName);

            await _imagesRepository.Remove(image);
            return Ok();
        }
        */

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            //var image = await _imagesRepository.GetById(id);
            var db = _imagesRepository.GetContext();
            var image = db.Images.FirstOrDefault(x => x.Id == id);

            if (image == null)
            {
                return NotFound();
            }

            //throw new System.Exception("Location: " + image.Location.ToString() + " Name: " + image.Name);
            
            FileHelper.DeleteFile(getDirectory(image.Location), image.Name);
            if (image.PreviewName != null)
                FileHelper.DeleteFile(getPreviewDirectory(image.Location), image.PreviewName);

            //image = await _imagesRepository.GetById(id);
            //await _imagesRepository.Remove(image);

            if (image != null)
                db.Images.Remove(image);

            await db.SaveChangesAsync();
            return Ok();
            
        }

        [HttpPatch("{location}/{id}")]
        public async Task<IActionResult> ToggleShow(Location location, int id)
        {
            var image = await _imagesRepository.GetById(id);
            if (image == null)
            {
                return NotFound();
            }          
            image.Show = !image.Show;
            await _imagesRepository.Update(image);
            return Ok();
        }

        [HttpPatch("sort/{location}")]
        public async Task<IActionResult> UpdateSortOrder(Location location, [FromBody]int[] ids)
        {
            if (ids != null && ids.Any())
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    var image = await _imagesRepository.GetById(ids[i]);
                    if (image != null && image.SortOrder != i)
                    {
                        image.SortOrder = i;
                        await _imagesRepository.Update(image);
                    }
                }
            }
            return Ok();
        }

        private string getDirectory(Location location)
        {
            return ImagesHelper.GetImagesDirectory(_appEnvironment.WebRootPath, location);
        }

        private string getPreviewDirectory(Location location)
        {
            return ImagesHelper.GetImagesPreviewDirectory(_appEnvironment.WebRootPath, location);
        }

    }
}
