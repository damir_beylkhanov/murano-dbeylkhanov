﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/applications")]
    public class JobApplicationsApiController: Controller
    {
        private readonly IRepository<JobApplication> _repository;

        public JobApplicationsApiController(IRepository<JobApplication> repository)
        {
            _repository = repository;
        }

        [HttpGet()]
        public Task<List<JobApplicationModel>> GetAll(Location? location, int? vacancyId)
        {
            IQueryable<JobApplication> applications = _repository.GetAll();
            if (location.HasValue)
            {
                applications = applications.Where(x => (x.Location & location) != 0);
            }
            if (vacancyId.HasValue)
            {
                applications = applications.Where(x => x.VacancyId == vacancyId);
            }

            return applications.OrderByDescending(x => x.CreatedOn).Select(x => new JobApplicationModel
            {
                Id = x.Id,
                VacancyId = x.VacancyId,
                FileName = x.FileName,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                Location = x.Location,
                Comment = x.Comment,
                Date = x.CreatedOn,
                VacancyName = x.Vacancy.Title
                
            }).ToListAsync();
        }

        [HttpGet("{id}/cv")]
        public async Task<IActionResult> GetCV(int id)
        {

            var application = await _repository.GetById(id);
            if (application != null)
            {
               return File(application.FileContent, FileHelper.GetContentType(application.FileName), application.FileName);
            }
            return NotFound();
        }
    }
}