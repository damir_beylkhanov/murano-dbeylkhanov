﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Config
{
    public class JobApplicationOptions
    {
        public string ValidExtensions { get; set; }

        public bool IsExtensionValid(string extension)
        {
            var validExtArr = ValidExtensions.ToLower().Split(';');
            return validExtArr.Contains(extension.ToLower());
        }
    }
}
