﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MuranoWeb.Data.Entity;

namespace MuranoWeb.Models.Home
{
    public class VacancyModel : Vacancy
    {
        public string IconUrl { get; set; }
    }
}
