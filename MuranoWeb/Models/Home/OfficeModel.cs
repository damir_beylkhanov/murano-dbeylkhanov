﻿using System.Collections.Generic;
using MuranoWeb.Data.Entity;

namespace MuranoWeb.Models.Home
{
    public class OfficeModel
    {
        public IEnumerable<VacancyModel> Vacancies { get; set; }
        public Quote Quote { get; set; }
        public Location Location { get; set; }
    }
}