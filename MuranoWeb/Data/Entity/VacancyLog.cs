﻿using System;

namespace MuranoWeb.Data.Entity
{
    public class VacancyLog
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }
        public string User { get; set; }
        public VacancyAction Action { get; set; }

        public int VacancyId { get; set; }
        public Vacancy Vacancy { get; set; }
    }

    public enum VacancyAction
    {
        Create = 1,
        Open = 2,
        Archive = 3,
        Update = 4
    }
}
