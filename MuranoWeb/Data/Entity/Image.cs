﻿using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class Image
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string PreviewName { get; set; }
        public bool Show { get; set; }
        public int SortOrder { get; set; }
        public Location Location { get; set; }
    }
}