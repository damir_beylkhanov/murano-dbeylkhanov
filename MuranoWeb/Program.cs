﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Data.Migrations;
using MuranoWeb.Data.Entity;

namespace MuranoWeb
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            SeedData(host);

            host.Run();
        }

        private static void SeedData(IWebHost host)
        {
            var services = (IServiceScopeFactory)host.Services.GetService(typeof(IServiceScopeFactory));

            using (IServiceScope scope = services.CreateScope())
            {
                VacanciesSeed.Seed(scope.ServiceProvider.GetRequiredService<IRepository<Vacancy>>());
                QuotesSeed.Seed(scope.ServiceProvider.GetRequiredService<IRepository<Quote>>());
                ImagesSeed.Seed(scope.ServiceProvider.GetRequiredService<IRepository<Image>>(), scope.ServiceProvider.GetRequiredService<IHostingEnvironment>());
            }
        }
    }
}
